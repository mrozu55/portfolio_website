import { Component, OnInit } from "@angular/core";

@Component({
  selector: "navigation",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"],
})
export class NavComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  changeWeight(evt) {
    var target = evt.target;
    if (target.id == "work_id") {
      (document.querySelectorAll(".navbar")[0] as HTMLElement).classList.add(
        "bold"
      );
      (document.querySelectorAll(".navbar")[1] as HTMLElement).classList.remove(
        "bold"
      );
      (document.querySelectorAll(".navbar")[2] as HTMLElement).classList.remove(
        "bold"
      );
    } else if (target.id == "about_id") {
      (document.querySelectorAll(".navbar")[0] as HTMLElement).classList.remove(
        "bold"
      );
      (document.querySelectorAll(".navbar")[1] as HTMLElement).classList.add(
        "bold"
      );
      (document.querySelectorAll(".navbar")[2] as HTMLElement).classList.remove(
        "bold"
      );
    } else if (target.id == "contact_id") {
      (document.querySelectorAll(".navbar")[0] as HTMLElement).classList.remove(
        "bold"
      );
      (document.querySelectorAll(".navbar")[1] as HTMLElement).classList.remove(
        "bold"
      );
      (document.querySelectorAll(".navbar")[2] as HTMLElement).classList.add(
        "bold"
      );
    }
  }
}
