import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  adress_:string = 'loremipsim 24, Madgin 10-423';
  phone_:string = '123456789';
  email_:string = 'lorem@ipsum.com';
  constructor() { }

  ngOnInit() {
  }

}
