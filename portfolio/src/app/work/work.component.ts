import { Component, OnInit } from '@angular/core';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit {


  
  constructor() { 
    
  }

  ngOnInit() {
    
  }
  

  hoverEffectOn(evt){
    evt.target.children[0].classList.add('descrip-after');
  }

  hoverEffectOff(evt){
    evt.target.children[0].classList.remove('descrip-after');
  }
  
  changeWork(evt){
    var target = evt.target;  
    if(target.id == 'two'){
      (document.querySelectorAll('.collection-item')[0] as HTMLElement).classList.remove('active');
      (document.querySelectorAll('.collection-item')[1] as HTMLElement).classList.add('active');  
      (document.querySelectorAll('.collection-item')[2] as HTMLElement).classList.remove('active');  
    }
    else if(target.id =='three'){
      (document.querySelectorAll('.collection-item')[2] as HTMLElement).classList.add('active');
      (document.querySelectorAll('.collection-item')[1] as HTMLElement).classList.remove('active');  
      (document.querySelectorAll('.collection-item')[0] as HTMLElement).classList.remove('active');  
    }
    else{
      (document.querySelectorAll('.collection-item')[1] as HTMLElement).classList.remove('active');
      (document.querySelectorAll('.collection-item')[2] as HTMLElement).classList.remove('active');  
      (document.querySelectorAll('.collection-item')[0] as HTMLElement).classList.add('active');  
    }
    


      
  }
}
